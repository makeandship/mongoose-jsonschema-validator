# Mongoose JSONSchema Validator

A Mongoose plugin to run validation through JSONSchema.

## Installation

`npm install --save mongoose-jsonschema-validator`
