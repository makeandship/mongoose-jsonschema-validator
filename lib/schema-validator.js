const mongoose = require("mongoose");
const traverse = require("traverse");
const Logger = require("makeandship-api-common/lib/modules/logging/Logger");
const { Validator } = require("makeandship-api-common/lib/modules/ajv");
const {
  SchemaExplorer,
} = require("makeandship-api-common/lib/modules/jsonschema");

// mongoose error container
const ValidationError = mongoose.Error.ValidationError;

// mongoose individual error for a path
const ValidatorError = mongoose.Error.ValidatorError;

/**
 * Format ajv errors into mongoose errors
 *
 * @param {*} document
 * @param {*} errors
 *
 * @return {ValidationError} error container
 */
const format = (document, errors, message, options = {}) => {
  if (errors) {
    const validationError = new ValidationError(document, {
      message,
    });

    Object.keys(errors).forEach((path) => {
      const error = errors[path];

      const { message, type } = error;

      const value =
        typeof document.get === "function"
          ? document.get(path)
          : document[path];

      const validatorError = new ValidatorError({ path, message, type, value });

      validationError.errors[path] = validatorError;
    });

    return validationError;
  }

  return null;
};

/**
 * Load a schema from a path or from a schema object
 * @param  {string|object} schema Can be the path or the schema itself
 * @return {obj}        The json schema
 * @ignore
 */
const loadSchema = (schema) =>
  typeof schema === "string" ? require(schema) : schema;

/**
 * Validate a mongoose model against a jsonschema
 * @param {object} schema  Mongoose schema
 * @param {object} options Options for validation
 * @param {string|object} options.jsonschema Absolute path of the json schema or the schema itself
 * @throws {Error} If there is error including he schema from a path
 */
const schemaValidator = (schema, options) => {
  options.jsonschema = loadSchema(options.jsonschema);

  schema.pre(
    "save",
    (function (options) {
      return function (next) {
        Logger.debug(`schemaValidator#pre:save: enter`);
        const explorer = new SchemaExplorer(options.jsonschema);
        Logger.debug(`schemaValidator#pre:save: explorer created`);
        const o = this.toObject();

        traverse(o).forEach(function (value) {
          if (typeof value === "object" && value instanceof Date) {
            const path = this.path.join(".");
            try {
              const iso =
                explorer.getFormat(path) === "date"
                  ? value.toISOString().substring(0, 10)
                  : value.toISOString();
              this.update(iso);
            } catch (e) {
              const iso = value.toString(); // let the ajv validation handle it
              this.update(iso);
            }
          }
        });
        Logger.debug(`schemaValidator#pre:save: traversed object`);

        // create the ajv instance
        const validator = new Validator(options.jsonschema);
        const errors = validator.validate(o);

        Logger.debug(`schemaValidator#pre:save: validated`);

        if (errors) {
          const name = options.modelName || "Document";
          const message = `Failed to save ${name}`;
          const error = format(this, errors, message, options);
          Logger.debug(`schemaValidator#pre:save: exit (errors)`);
          next(error);
        } else {
          Logger.debug(`schemaValidator#pre:save: exit (no errors)`);
          next();
        }
      };
    })(options)
  );

  schema.pre(
    "update",
    (function (options) {
      return function (next) {
        const explorer = new SchemaExplorer(options.jsonschema);
        const o = this.getUpdate();

        traverse(o).forEach(function (value) {
          if (typeof value === "object" && value instanceof Date) {
            const path = this.path.join(".");
            try {
              const iso =
                explorer.getFormat(path) === "date"
                  ? value.toISOString.substring(0, 10)
                  : value.toISOString();
              this.update(iso);
            } catch (e) {
              const iso = value.toString(); // let the ajv validation handle it
              this.update(iso);
            }
          }
        });

        // create the ajv instance
        const validator = new Validator(options.jsonschema);
        const errors = validator.validate(o);
        if (errors) {
          const name = options.modelName || "Document";
          const message = `Failed to update ${name}`;
          const error = format(this, errors, message, options);

          next(error);
        } else {
          next();
        }
      };
    })(options)
  );
};

module.exports = schemaValidator;
